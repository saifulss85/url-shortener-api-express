# URL Shortener

## Quickstart

1) Copy `.env.example` to `.env` - edit the values as necessary, taking cues from the examples therein.
2) Run `docker-compose up -d` to bring up all the Docker containers.
3) On your browser, go to http://localhost:8080 - this is the Nginx container, proxying all requests to the running Express server running in a separate container.
4) To browse the contents of the underlying MySQL database, go to http://localhost:8081 - this is phpMyAdmin, a web app that lets you browse MySQL databases.
5) Run migrations, which creates the relational schema in our MySQL container: `docker-compose exec app npm run db:migrate`

The app is now up and running. But this repo is a work-in-progress REST API, so there isn't much of a UI for it. Further interactions are meant to be done in a REST client, like Postman.

For your convenience, the repo includes a Postman collection and the associated Postman environment you can import into Postman, so you can see the various endpoints in the app.