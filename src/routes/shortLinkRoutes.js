const models = require('../models');
const { Click, ShortLink } = models;
const ShortLinkPojo = require('../models/pojos/ShortLinkPojo');

/**
 *
 * @param {Express} app
 */
module.exports = app => {
  app.get('/api/v1/short-links', async (req, res) => {
    const shortLinks = await ShortLink.findAll({
      include: [{
        model: Click,
        as: 'clicks',
      }]
    });

    res.send(shortLinks);
  });

  app.get('/api/v1/short-links/:short_link_url', async (req, res) => {
    const shortLinkUrl = req.params.short_link_url;

    const shortLink = await ShortLink.findOne({
      where: {
        short_url: shortLinkUrl
      },
      include: [{
        model: Click,
        as: 'clicks',
      }]
    });

    if (!shortLink) res.status(404).send({ message: `Record ${shortLinkUrl} not found` });
    else {
      await Click.create({
        short_link_id: shortLink.id,
        client_remote_ip: null
      });

      res.send(shortLink);
    }
  });

  app.post('/api/v1/short-links', async (req, res) => {
    const shortLinkPojo = new ShortLinkPojo(req.body.longLink);

    let shortLink;

    let count = 0;

    while (!shortLink) {
      count++;

      try {
        shortLink = await ShortLink.create({
          short_url: shortLinkPojo.currentShortLinkCandidate,
          long_url: shortLinkPojo.longLink,
        });
      } catch (e) {
        console.log(`Try number ${count} failed`);
        shortLinkPojo.nextShortLinkCandidate();
      }
    }

    res.send(shortLink);
  });
};