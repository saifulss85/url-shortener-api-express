const passport = require('passport');

/**
 *
 * @param {Express} app
 */
module.exports = app => {
  app.get(
    '/auth/google',
    passport.authenticate('google', {
      scope: ['profile', 'email']
    }),
  );

  app.get(
    '/auth/google/callback',
    passport.authenticate('google'),
    (req, res) => {
      console.log('/auth/google/callback found this user:', req.user);
      res.redirect('/');
    }
  );

  app.get('/api/logout', (req, res) => {
    req.logout();   // Passport auto-attaches this function to the request object
    res.redirect('/');
  });

  app.get('/api/current_user', (req, res) => {
    console.log(req);
    console.log(req.user);

    const util = require('util');
    res.send(util.inspect(req));
  });
};