const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
const googleClientSecret = require('../config/keys').googleClientSecret;
const googleClientID = require('../config/keys').googleClientID;

// const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  done(null, {
    id: '1',
    name: 'saiful',
    email: 'saifulss85@gmail.com',
  });
});

passport.use(
  new GoogleStrategy(
    {
      clientID: googleClientID,
      clientSecret: googleClientSecret,
      callbackURL: '/auth/google/callback',
    }, createUserIfNotExists)
);

/**
 * Here is where you specify the custom code you want to run, once a Google auth request completes successfully
 * and Google redirects the user back to your callback URL. You would usually want to create a user record
 * in your app's persistence layer here, for example.
 *
 * @param accessToken
 * @param refreshToken
 * @param profile
 * @param done
 */
function createUserIfNotExists(accessToken, refreshToken, profile, done) {
  // do whatever you want with the access token and profile, then call the done function with the user object in second argument

  const user = {
    id: profile.id,
    name: profile.displayName,
    email: profile.emails[0].value,
    accessToken,
  };

  done(null, user);

  // User.findOne({ googleId: profile.id })
  //   .then(existingUser => {
  //     if (existingUser) {
  //       console.log('user already exists', existingUser);
  //       done(null, existingUser);
  //     } else {
  //       new User({ googleId: profile.id, })
  //         .save()
  //         .then(newlyCreatedUser => done(null, newlyCreatedUser));
  //     }
  //   });
}