const Sequelize = require('sequelize');
const config = require('../config/config');

const database = config.development.database;
const username = config.development.username;
const password = config.development.password;
const host = config.development.host;
const dialect = config.development.dialect;

const sequelize = new Sequelize(database, username, password, {
  host,
  dialect
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully!');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });