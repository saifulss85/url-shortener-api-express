const crypto = require('crypto');
const base62 = require('base62');

console.log(base62.encode(Number.MAX_SAFE_INTEGER));
console.log(Number.MAX_SAFE_INTEGER.toString().length);
// console.log(base62.decode('ZZZZZZ').toString().length);
// console.log(base62.decode('ZZZZZZZ').toString().length);
console.log(base62.decode('ZZZZZZZZ').toString().length);

// const url = 'http://saifulshahril.com/hello';
// const hashed = hash(url);
// const encoded = encode(hashed);
//
// console.log(hashed, hashed.length);
//
// console.log(encoded, encoded.length);

function hash(url) {
  return crypto.createHash('md5')
    .update(url)
    .digest('hex');
}

function encode(normalString) {
  const buffer = Buffer.from(normalString);
  return buffer.toString('base64');
}

function decode(encodedString) {
  const buffer = Buffer.from(encodedString, 'base64');
  return buffer.toString('ascii');
}