'use strict';
module.exports = (sequelize, DataTypes) => {
  const Click = sequelize.define('Click', {
    short_link_id: DataTypes.INTEGER,
    client_remote_ip: DataTypes.STRING,
  }, {
    tableName: 'clicks',
    createdAt: 'created_at',
    updatedAt: false,
    sequelize
  });

  Click.associate = function (models) {
    Click.belongsTo(models.ShortLink, {
      foreignKey: 'short_link_id',
      targetKey: 'id',
    });
  };

  return Click;
};