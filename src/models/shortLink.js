'use strict';

module.exports = (sequelize, DataTypes) => {
  const ShortLink = sequelize.define('ShortLink', {
    short_url: DataTypes.STRING,
    long_url: DataTypes.STRING,
    created_by: DataTypes.INTEGER,
  }, {
    tableName: 'short_links',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    sequelize
  });

  ShortLink.associate = function (models) {
    ShortLink.belongsTo(models.User, {
      foreignKey: 'created_by',
    });

    ShortLink.hasMany(models.Click, {
      foreignKey: 'short_link_id',
      sourceKey: 'id',
      as: 'clicks',
    });
  };

  return ShortLink;
};