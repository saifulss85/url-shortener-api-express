const ShortLinkPojo = require('../pojos/ShortLinkPojo');

describe('ShortLinkPojo', () => {
  let shortLinkPojo;
  let candidate1;
  let candidate2;
  let candidate3;
  let candidate4;
  let candidate5;

  beforeEach(() => {
    shortLinkPojo = new ShortLinkPojo('https://example.com');
    candidate1 = shortLinkPojo.currentMd5Hash.substring(0, 6);
    candidate2 = shortLinkPojo.currentMd5Hash.substring(6, 12);
    candidate3 = shortLinkPojo.currentMd5Hash.substring(12, 18);
    candidate4 = shortLinkPojo.currentMd5Hash.substring(18, 24);
    candidate5 = shortLinkPojo.currentMd5Hash.substring(24, 30);
  });

  test('it gets the correct 1st candidate short link', () => {
    expect(shortLinkPojo.currentShortLinkCandidate).toBe(candidate1);
  });

  test('it gets the correct 2nd candidate short link', () => {
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 2
    expect(shortLinkPojo.currentShortLinkCandidate).toBe(candidate2);
  });

  test('it gets the correct 3rd candidate short link', () => {
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 2
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 3
    expect(shortLinkPojo.currentShortLinkCandidate).toBe(candidate3);
  });

  test('it gets the correct 4th candidate short link', () => {
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 2
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 3
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 4
    expect(shortLinkPojo.currentShortLinkCandidate).toBe(candidate4);
  });

  test('it gets the correct 5th candidate short link', () => {
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 2
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 3
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 4
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 5
    expect(shortLinkPojo.currentShortLinkCandidate).toBe(candidate5);
  });

  test('it gets the correct 6th candidate short link', () => {
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 2
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 3
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 4
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 5

    const currentMd5Hash = shortLinkPojo.currentMd5Hash;
    shortLinkPojo.nextShortLinkCandidate(); // now candidate 6
    const newMd5Hash = shortLinkPojo.currentMd5Hash;

    expect(newMd5Hash).not.toBe(currentMd5Hash);

    expect(shortLinkPojo.currentShortLinkCandidate).toBe(newMd5Hash.substring(0, 6));
  });
});