const NUM_CHARS_IN_SHORT_LINK = process.env.NUM_CHARS_IN_SHORT_LINK || 6;

class ShortLinkPojo {
  /**
   *
   * @param {string} longLink
   */
  constructor(longLink) {
    this.longLink = longLink;
    this.currentMd5Hash = ShortLinkPojo.createHash(this.longLink);
    this.currentCandidateIndex = 0;
    this.nextShortLinkCandidate();
  }

  /**
   *
   * @param {string} string
   */
  static createHash(string) {
    const crypto = require('crypto');
    return crypto.createHash('md5').update(string).digest("hex");
  }

  nextShortLinkCandidate() {
    // checks currentCandidateIndex
    // if 1, take the first NUM_CHARS_IN_SHORT_LINK of currentMd5Hash and set currentShortLinkCandidate to this
    // if 2, take the second NUM_CHARS_IN_SHORT_LINK of currentMd5Hash and set currentShortLinkCandidate to this
    // ...
    // if next candidateIndex is a multiple of 6, add uuid to long link, re-md5 it and set currentMd5Hash to this new md5

    if (!this.currentMd5Hash) throw new Error('Current md5 hash has not been set!');

    if ((this.currentCandidateIndex + 1) % 6 === 0) {
      const uuid = require('uuid');
      const someUuid = uuid.v1();
      this.currentMd5Hash = ShortLinkPojo.createHash(this.longLink + someUuid);
    }

    const startIndex = (this.currentCandidateIndex % 5) * NUM_CHARS_IN_SHORT_LINK;
    const endIndex = startIndex + NUM_CHARS_IN_SHORT_LINK;
    this.currentShortLinkCandidate = this.currentMd5Hash.substring(startIndex, endIndex);
    this.currentCandidateIndex += 1;
  }
}

module.exports = ShortLinkPojo;