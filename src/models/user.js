'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
  }, {
    tableName: 'users',
    createdAt: 'created_at',
    sequelize
  });

  User.associate = function(models) {
    User.hasMany(models.ShortLink, {
      foreignKey: 'created_by',
    });
  };

  return User;
};