require('dotenv').config();
const express = require('express');
const cors = require('cors');
// const passport = require('passport');
const bodyParser = require('body-parser');
// require('./services/passport');

const app = express();

app.use(cors());
app.use(bodyParser.json());
// app.use(passport.initialize());
// app.use(passport.session());

app.get('/', (req, res) => {
  req.user ? console.log('User found: ', req.user) : console.log('No user found');
  req.logout ? console.log('Logout function is there') : console.log('No logout function found');

  // console.log('This is the Awesome URL Shortener backend.');
  res.send('This is the Awesome URL Shortener backend. Click to log in using Google: <a href="/auth/google">Sign in</a><a href="/api/current_user">See user</a>');
});

// require('./routes/authRoutes')(app);
require('./routes/shortLinkRoutes')(app);

const PORT = process.env.PORT || 3000;

if (!process.env.DONOTRUN) {
  require('./services/initDatabase');
  app.listen(PORT);
  console.log(`Express started! Now listening to port ${PORT}.`);
}